#include "Bezier.h"
#include <iostream>
using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	float t, xt, yt;
	float step = 0.0001;
	t = 0;
	while(t<=1)
	{
		xt =(float) (1 - t)*(1 - t)*p1.x + 2 * (1 - t)*t*p2.x + t*t*p3.x;
		yt =(float) (1 - t)*(1 - t)*p1.y + 2 * (1 - t)*t*p2.y + t*t*p3.y;
		SDL_RenderDrawPoint(ren, (int)xt, (int)yt);
		t += step;
	}
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	float t, xt, yt;
	float step = 0.0001;
	t = 0;
	while (t <= 1)
	{
		xt = (float)(1 - t)*(1 - t)*(1 - t)*p1.x + 3 * (1 - t)*(1 - t) *t*p2.x + 3 * (1 - t)*t*t*p3.x + t*t*t*p4.x;
		yt = (float)(1 - t)*(1 - t)*(1 - t)*p1.y + 3 * (1 - t)*(1 - t) *t*p2.y + 3 * (1 - t)*t*t*p3.y + t*t*t*p4.y;
		SDL_RenderDrawPoint(ren, (int)xt, (int)yt);
		t += step;
	}
}


